# E-commerce Webjump

# Sobre o projeto

Esta aplicação é um modelo de e-commerce que faz requisições para uma API no backend a fim de obter uma lista de categorias e uma lista de produtos. Os menus são gerados dinamicamente conforme as categorias obtidas no backend. Ao clicar nas categorias nos menus, o usuário é direcionado para a página de catálogo onde os produtos são renderizados em forma de grid ou lista, conforme a escolha do usuário.

# Tecnologias utilizadas
* HTML 
* CSS 
* JavaScript
* ReactJS

## Dependências
* Node-sass: responsável pelo processamento dos arquivos .scss
* Axios: responsável pelas requisições ao backend
* Concurrently: usado para iniciar o frontend e o backend ao mesmo tempo, no mesmo terminal, com apenas um comando
* React router dom: utilizado para redirecionamento e envio de dados para outros componentes através da tag 'Link'

# Como executar o projeto

Pré-requisitos: Node.js, npm/yarn

```bash
# clonar repositório
git clone https://bitbucket.org/jeangregolon/assessment-frontend.git

# instalar dependências
npm install

# executar o projeto
npm run start
```

# Melhorias futuras
* Implementar a ordenação dos produtos e obter as opções do select dinamicamente
* Implementar filtro por cor e tipo obtidos dinamicamente

# Autor

Jean Gregolon

https://www.linkedin.com/in/jeangregolon/
