import React from 'react'
import './homescreen.scss'
import CategoryList from '../../components/CategoryList/CategoryList'

export default function HomeScreen() {

  return (
    <div className='page'>
      <div className='side-menu'>
        <ul>
          <li><a href="/">Página inicial</a></li>
          <CategoryList />
          <li><a href="/contato">Contato</a></li>
        </ul>
      </div>
      <div className='content'>
        <div className='rectangle'></div>
        <div className='text'>
          <h1>Seja bem-vindo!</h1>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            Phasellus pharetra ultricies augue, in molestie orci porttitor at.
            Maecenas et quam vitae urna convallis egestas nec in turpis. Sed in
            mi risus. Quisque porttitor fringilla nisl et tincidunt. Proin orci risus,
            condimentum vitae lectus non, elementum malesuada risus. Aenean non lobortis
            purus, eget laoreet dolor. Nam dictum condimentum rhoncus. Mauris facilisis
            pellentesque tristique. Aliquam dui dui, dignissim vel mattis et, fermentum
            eu sapien. Ut tempor auctor nisi. Mauris ultrices gravida justo, nec aliquet
            diam convallis ac. Etiam vel neque pellentesque, gravida orci ac, blandit est.
            Nunc a imperdiet velit. In placerat, nisi vulputate tincidunt cursus, turpis
            purus ultrices dui, a aliquam leo nisl eget erat. Nullam a purus nisi.
            Aenean ut auctor diam. Vivamus luctus mi eget nisl rutrum, quis dictum magna
            feugiat. Quisque efficitur urna sit amet metus aliquam facilisis. Duis vel
            auctor lorem, vel semper lectus. Ut dignissim tristique.</p>
        </div>
      </div>
    </div>
  )
}
