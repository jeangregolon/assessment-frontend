import React, { useState, useEffect } from 'react'
import Axios from 'axios'
import ProductCard from '../../components/ProductCard/ProductCard'
import CategoryList from '../../components/CategoryList/CategoryList'
import './catalogScreen.scss'

export default function CatalogScreen(props) {
  const [view, setView] = useState('grid')
  const [loading, setLoading] = useState(true)
  const [products, setProducts] = useState('')
  const categoryId = props.location.state.id
  const categoryName = props.location.state.name
  const pageTitle = categoryName.charAt(0).toUpperCase() + categoryName.slice(1)

  useEffect(() => {
    setLoading(true)
    Axios.get(`http://localhost:8888/api/V1/categories/${categoryId}`)
      .then(res => {
        let products = res.data.items
        setProducts(products)
        setLoading(false)
      })
  }, [categoryId])

  return (
    <div className='page'>
      <div className='filter'>
        <h2>Filtre por</h2>
        <h3>Categorias</h3>
        <ul>
          <CategoryList />
        </ul>

        <h3>Cores</h3>
        <div className='color-box red'><a href="/red"> </a></div>
        <div className='color-box orange'><a href="/"> </a></div>
        <div className='color-box blue'><a href="/"> </a></div>

        <h3>Tipo</h3>
        <ul>
          <li><a href="/">Corrida</a></li>
          <li><a href="/">Caminhada</a></li>
          <li><a href="/">Casual</a></li>
          <li><a href="/">Social</a></li>
        </ul>
      </div>
      <div className='results'>
        <p className='page-title'>{pageTitle}</p>
        <div className='topbar'>

          <div className='layout-icons'>
            <i className={`fa fa-th fa-lg ${view === 'grid' ? 'active' : ''}`}
              onClick={() => setView('grid')}></i>
            <i className={`fa fa-th-list fa-lg ${view === 'list' ? 'active' : ''}`}
              onClick={() => setView('list')}></i>
          </div>

          <div className='order-by'>
            <p>Ordenar Por</p>
            <select name="" id="">
              <option value="maior-preco">Preço</option>
            </select>
          </div>

        </div>

        <div className='products'>
          {!loading ? (products.map(product => (
            <ProductCard key={product.id} style={view} product={product} />
          ))) : (<h1>Carregando...</h1>)}
        </div>
      </div>
    </div>
  )
}
