import React from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import './app.scss'

// Screens
import HomeScreen from './screens/HomeScreen/HomeScreen'
import CatalogScreen from './screens/CatalogScreen/CatalogScreen'

// Components
import Header from './components/Header/Header'
import Footer from './components/Footer/Footer'

function App() {

  return (
    <BrowserRouter>
      <div>
        <main>
          <Header />

          <div className='pages'>
            <Route path='/:path' component={CatalogScreen} />
            <Route path='/' component={HomeScreen} exact />
          </div>

          <Footer />
        </main>
      </div>

    </BrowserRouter>
  )
}

export default App