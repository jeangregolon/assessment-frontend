import React, { useState } from 'react'
import CategoryList from '../CategoryList/CategoryList'
import './header.scss'

export default function Header() {
  let [toggle, setToggle] = useState(false)

  const menuToggle = () => {
    if (!toggle) {
      setToggle(true)
    } else if (toggle) {
      setToggle(false)
    }
  }

  return (
    <header>
      <div className='login-area'>
        <p><a href="/">Acesse sua Conta</a> ou <a href="/">Cadastre-se</a></p>
      </div>
      <div className="header">
        <i className='fa fa-bars fa-2x hamburger' onClick={menuToggle}></i>
        <a href="/" className="brand"><img src="/media/logo_webjump.png" alt="webjump" /></a>
        <span className='desktop-search'>
          <input type="text" className='search-input' />
          <button className='search-button'>Buscar</button>
        </span>
        <i className='fa fa-search fa-2x search-icon'></i>
      </div>
      <div>
        <nav className="nav">
          <ul className={`nav-menu ${!toggle ? 'nav-hide' : ''}`}>
            <li><a href="/">Página inicial</a></li>
            <CategoryList />
            <li><a href="/contato">Contato</a></li>
          </ul>
        </nav>
      </div>
    </header>
  )
}

