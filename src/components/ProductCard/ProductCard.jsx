import React from 'react'
import './productCard.scss'

export default function Product(props) {
  const { product } = props

  return (
    <div className={props.style} id={product.id}>
      <div className={`${props.style}-container`}>
        <img className='image' src={product.image} alt={product.name} />
        <p className={`${props.style}-name name`}>{product.name}</p>

        {product.specialPrice ?
          (
            <div className="price">
              <div className="crossed">R${product.price.toFixed(2).toString().replace(".", ",")}</div>
              <div >R${product.specialPrice.toFixed(2).toString().replace(".", ",")}</div>
            </div>
          )
          :
          (
            <div className={`${props.style}-no-sale price`}>R${product.price.toFixed(2).toString().replace(".", ",")}</div>
          )}
        <button className={`${props.style}-btn btn`}>Comprar</button>
        <button className='list-mobile-button btn'><i className='fa fa-shopping-cart'></i></button>

      </div>
    </div>
  )
}