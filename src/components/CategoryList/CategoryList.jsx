import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import Axios from 'axios'

export default function CategoryList() {
  let [loading, setLoading] = useState(true)
  let [categories, setCategories] = useState('')

  useEffect(() => {
    setLoading(true)
    Axios.get("http://localhost:8888/api/V1/categories/list")
      .then(res => {
        let categories = res.data.items
        setCategories(categories)
        setLoading(false)
      })
  }, [])

  return (
    <>
      {!loading ? (categories.map(category => (
        <li key={category.id}>
          <Link to={{ pathname: `/${category.path}`, state: { name: category.name, id: category.id } }}>
            {category.name}
          </Link>
        </li>
      ))) : ('')}
    </>
  )
}